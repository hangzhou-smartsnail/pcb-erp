package com.ruoyi.erp.mapper;

import java.util.List;
import com.ruoyi.erp.domain.ErpClassifyModeAttributeRt;

/**
 * 分类型号属性关联Mapper接口
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public interface ErpClassifyModeAttributeRtMapper 
{
    /**
     * 查询分类型号属性关联
     * 
     * @param id 分类型号属性关联ID
     * @return 分类型号属性关联
     */
    public ErpClassifyModeAttributeRt selectErpClassifyModeAttributeRtById(String id);

    /**
     * 查询分类型号属性关联列表
     * 
     * @param erpClassifyModeAttributeRt 分类型号属性关联
     * @return 分类型号属性关联集合
     */
    public List<ErpClassifyModeAttributeRt> selectErpClassifyModeAttributeRtList(ErpClassifyModeAttributeRt erpClassifyModeAttributeRt);

    /**
     * 新增分类型号属性关联
     * 
     * @param erpClassifyModeAttributeRt 分类型号属性关联
     * @return 结果
     */
    public int insertErpClassifyModeAttributeRt(ErpClassifyModeAttributeRt erpClassifyModeAttributeRt);

    /**
     * 修改分类型号属性关联
     * 
     * @param erpClassifyModeAttributeRt 分类型号属性关联
     * @return 结果
     */
    public int updateErpClassifyModeAttributeRt(ErpClassifyModeAttributeRt erpClassifyModeAttributeRt);

    /**
     * 删除分类型号属性关联
     * 
     * @param id 分类型号属性关联ID
     * @return 结果
     */
    public int deleteErpClassifyModeAttributeRtById(String id);

    /**
     * 批量删除分类型号属性关联
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteErpClassifyModeAttributeRtByIds(String[] ids);
}
