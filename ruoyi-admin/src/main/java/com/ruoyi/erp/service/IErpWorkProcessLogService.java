package com.ruoyi.erp.service;

import java.util.List;
import com.ruoyi.erp.domain.ErpWorkProcessLog;

/**
 * 生产进度日志Service接口
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public interface IErpWorkProcessLogService 
{
    /**
     * 查询生产进度日志
     * 
     * @param id 生产进度日志ID
     * @return 生产进度日志
     */
    public ErpWorkProcessLog selectErpWorkProcessLogById(String id);

    /**
     * 查询生产进度日志列表
     * 
     * @param erpWorkProcessLog 生产进度日志
     * @return 生产进度日志集合
     */
    public List<ErpWorkProcessLog> selectErpWorkProcessLogList(ErpWorkProcessLog erpWorkProcessLog);

    /**
     * 新增生产进度日志
     * 
     * @param erpWorkProcessLog 生产进度日志
     * @return 结果
     */
    public int insertErpWorkProcessLog(ErpWorkProcessLog erpWorkProcessLog);

    /**
     * 修改生产进度日志
     * 
     * @param erpWorkProcessLog 生产进度日志
     * @return 结果
     */
    public int updateErpWorkProcessLog(ErpWorkProcessLog erpWorkProcessLog);

    /**
     * 批量删除生产进度日志
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteErpWorkProcessLogByIds(String ids);

    /**
     * 删除生产进度日志信息
     * 
     * @param id 生产进度日志ID
     * @return 结果
     */
    public int deleteErpWorkProcessLogById(String id);
}
